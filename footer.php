<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Twice_Agency
 */


if (pll_current_language() == 'fr') :
	$lifestyle = 'twice-lifestyle-3';
	$events = 'twice-events-3';
	$estates = 'twice-estates-3';
	$blog = 'blog-2';
	$careers = 'careers-fr';
	$terms = 'termes-et-conditions';
	$cookies = 'cookie-policy-fr';
	$privacy = 'politique-de-confidentialite';

else :
	$lifestyle = 'twice-lifestyle';
	$events = 'twice-events';
	$estates = 'twice-estates';
	$blog = 'blog';
	$careers = 'careers';
	$terms = 'terms-conditions';
	$cookies = 'cookie-policy';
	$privacy = 'privacy-policy';
endif;


?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info section">
			<div class="columns is-centered">
				<div class="column is-10 is-10-desktop is-12-mobile is-12-tablet">
					<div class="columns is-multiline justify-sb footer__col">
						<div class="column is-narrow is-paddingless ">
							<p class="footer__title"><?php pll_e('Contact'); ?></p>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col is-vcentered">
								<div class="column is-narrow-mobile is-paddingless-lr-mobile"><i class="fas fa-phone icon--orange"></i>&nbsp;&nbsp;UK <a href="tel:+4402034111876">+44 (0)203 4111 876</a> </div>
							</div>
							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col is-vcentered">
								<div class="column is-narrow-mobile is-paddingless-lr-mobile"><i class="fas fa-phone icon--orange"></i>&nbsp;&nbsp;FR <a href="tel:+330422840013">+33 (0)422 8400 13</a></div>
							</div>
							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col is-vcentered">
								<div class="column is-narrow-mobile is-paddingless-lr-mobile"><i class="fas fa-envelope icon--orange"></i>&nbsp;&nbsp;<a href="mailto:info@twice-agency.com">info@twice-agency.com</a></div>
							</div>
							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile is-paddingless-lr-mobile"><i class="fas fa-map-marker-alt icon--orange"></i>&nbsp;&nbsp;<a href="https://www.google.com/maps/place/Twice+Agency+-+Lifestyle+%26+Events/@51.5169074,-0.1563661,15z/data=!4m5!3m4!1s0x0:0xe96f0522417e0bd!8m2!3d51.5169074!4d-0.1563661" target="_blank">22-25 Portman Close<br />Marylebone, London<br /> W1H 6BS</a>
								</div>
							</div>

						</div>	<!-- .column.is-narrow is-paddingless -->
					
						<div class="column is-narrow is-paddingless is-hidden-touch footer-sep"><img src="/wp-content/uploads/footer-seperator.png"></div>

						<div class="column is-narrow is-paddingless ">
							<p class="footer__title">TWICE</p>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo pll_home_url(); ?>" title="Twice Agency"><?php pll_e('Home'); ?></a>
								</div>
							</div>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo pll_home_url() . $lifestyle; ?>" title="Twice Lifestyle">Lifestyle</a>
								</div>
							</div>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo pll_home_url() . $events; ?>" title="Twice Events">Events</a>
								</div>
							</div>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo pll_home_url() . $estates; ?>" title="Twice Estates">Estates</a>
								</div>
							</div>

						</div>	<!-- .column.is-narrow is-paddingless -->

						<div class="column is-narrow is-paddingless is-hidden-touch footer-sep"><img src="/wp-content/uploads/footer-seperator.png"></div>


						<div class="column is-narrow is-paddingless ">
							<p class="footer__title"><?php pll_e('Follow Us'); ?></p>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo pll_home_url() . $blog; ?>" target="_blank" title="Twice Agency Blog">Blog</a>
								</div>
							</div>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo get_field('gc_linkedin', 'option'); ?>" target="_blank" title="Twice Agency LinkedIn">LinkedIn</a>
								</div>
							</div>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo get_field('gc_instagram', 'option'); ?>" target="_blank" title="Twice Agency Instagram">Instagram</a>
								</div>
							</div>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo get_field('gc_facebook', 'option'); ?>" target="_blank" title="Twice Agency Facebook">Facebook</a>
								</div>
							</div>

						</div>	<!-- .column.is-narrow is-paddingless -->

						<div class="column is-narrow is-paddingless is-hidden-touch footer-sep"><img src="/wp-content/uploads/footer-seperator.png"></div>

						<div class="column is-narrow is-paddingless ">
							<p class="footer__title"><?php pll_e('More Info'); ?></p>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo pll_home_url() . $careers; ?>" title="Twice Agency LinkedIn"><?php pll_e('Careers') ?></a>
								</div>
							</div>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo pll_home_url() . $terms; ?>" title="Twice Agency Instagram"><?php pll_e('Terms and Conditions'); ?> </a>
								</div>
							</div>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo pll_home_url() . $cookies; ?>" title="Twice Agency Instagram">Cookies</a>
								</div>
							</div>

							<div class="columns is-flex is-justify-center-mobile is-mobile footer__col">
								<div class="column is-narrow-mobile footer__link">
									<a href="<?php echo pll_home_url() . $privacy; ?>" title="Twice Agency Facebook"><?php pll_e('Privacy') ?></a>
								</div>
							</div>

						</div>	<!-- .column.is-narrow is-paddingless -->
					</div> <!-- .columns -->

					<div class="columns has-text-centered">
						<div class="column is-full">&copy; Twice Agency <?php echo date("Y"); ?></div>
					</div>
				</div> <!-- .column.is-10 -->

			</div> <!-- .columns -->
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

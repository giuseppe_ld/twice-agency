<?php
/**
 * Twice Agency functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Twice_Agency
 */

if ( ! function_exists( 'twice_agency_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function twice_agency_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twice Agency, use a find and replace
		 * to change 'twice-agency' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'twice-agency', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'twice-agency' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'twice_agency_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'twice_agency_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function twice_agency_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'twice_agency_content_width', 640 );
}
add_action( 'after_setup_theme', 'twice_agency_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twice_agency_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'twice-agency' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'twice-agency' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twice_agency_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function twice_agency_scripts() {
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), false );
	
	wp_enqueue_style( 'owl-carousel-theme', get_template_directory_uri() . '/css/owl.theme.default.min.css', array(), false );

	wp_enqueue_style( 'twice-agency-style', get_stylesheet_uri() );

	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Lato:400,700', false );
	
	wp_enqueue_script( 'twice-agency-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	
	wp_enqueue_script( 'twice-agency-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/js/util/owl.carousel.min.js', array(), '20151215', true );
	
	wp_enqueue_script( 'twice-app', get_template_directory_uri() . '/js/app.js', array('jquery'), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'twice_agency_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/** Set time zone */
date_default_timezone_set('Europe/London');

/** Print_r array to page with styling */
function code($arr) {
	echo "<pre><code>";
	print_r($arr);
	echo "</pre></code>";
}

function console($arr) { ?>
	
	<script type="text/javascript">
		var arr = <?php echo json_encode($arr); ?>;
		console.log(arr);
	</script>
<?php }

function console_acf() { 
	
$x = get_fields(); ?>

	<script type="text/javascript">
		var arr = <?php echo json_encode($x); ?>;
		console.log(arr);
	</script>
<?php }

/** Takes array, returns comma split list */
function array_to_list($arrs) {
	echo implode(', ', $arrs);
}

/** Takes array, returns splace split list */
function array_to_classes($arrs) {
	echo implode(' ', $arrs);
}

/** Add Page slug to body class */
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

/**
 * Add ACF option page
 */

if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title' => 'General Content',
		'menu_title' => 'General Content',
		'icon_url' => 'dashicons-text',
	));


}

/**
 * Custom Post Types - Testimonials, Social Posts
 */

// Register Custom Post Types
add_action('init', 'register_custom_posts_init');

function register_custom_posts_init() {
    // Testimonials
    $testimonials_labels = array(
        'name'               => 'Testimonials',
        'singular_name'      => 'Testimonial',
				'menu_name'          => 'Testimonials',
				'add_new_item'          => 'Add new Testimonial',
				'add_new'          => 'Add new Testimonial',
		
    );
    register_post_type('testimonials', array(
        'labels'             => $testimonials_labels,
        'public'             => true,
        'capability_type'    => 'post',
				'has_archive'        => false,
				'exclude_from_search' => true,
				// 'menu_position'      => 23,
				'menu_icon'          => 'dashicons-editor-quote',
        'supports'           => array( 'title', 'revisions', 'editor', 'page-attributes' )
			)
		);

		// Our Team
    $our_team_labels = array(
        'name'               => 'Team Members',
        'singular_name'      => 'Team Member',
				'menu_name'          => 'Our Team',
				'add_new_item'          => 'Add Team Member',
				'add_new'          => 'Add Team Member',
		
    );
    register_post_type('teammembers', array(
        'labels'             => $our_team_labels,
        'public'             => true,
        'capability_type'    => 'post',
				'has_archive'        => false,
				'exclude_from_search' => true,
				// 'menu_position'      => 23,
				'menu_icon'          => 'dashicons-groups',
        'supports'           => array( 'title', 'revisions', 'page-attributes' )
			)
		);
}

//Function for Twice Sep
function twice_sep() {
	echo '<img src="/wp-content/uploads/twice-sep.png" class="twice-sep"/>';
}

//Function for Twice Sep Small
function twice_sep_small() {
	echo '<img src="/wp-content/uploads/Asset-2@2x.png" class="twice-sep-small"/>';
}

//Register our sidebars and widgetized areas.

function twice_widgets_init() {

	register_sidebar( array(
		'name'          => 'Select Language',
		'id'            => 'select_language',
	) );

}
add_action( 'widgets_init', 'twice_widgets_init' );

//CTA for certain pages

function enquire_cta() { 
	global $phone; ?>
<div class="landing-cta">
	<div class="level is-mobile">
		<div class="level-left">
			<div class="level-item">
				<div><a href="tel:<?php echo get_field($phone, 'option'); ?>"><?php echo get_field($phone, 'option'); ?></a></div>
			</div>
		</div>
		<div class="level-right">
			<div class="level-item">
				<div><a href="<?php echo lang_link('/contact'); ?>"><?php pll_e('Enquire Now'); ?></a></div>
			</div>
		</div>
	</div>
</div>
<?php 
}

function member_cta() { 
	global $phone; ?>
<div class="landing-cta">
	<div class="level is-mobile">
		<div class="level-left">
			<div class="level-item">
				<div><a href="tel:<?php echo get_field($phone, 'option'); ?>"><?php echo get_field($phone, 'option'); ?></a></div>
			</div>
		</div>
		<div class="level-right">
			<div class="level-item">
				<div><a href="<?php echo lang_link('/twice-lifestyle/become-a-member'); ?>"><?php pll_e('Become a member'); ?></a></div>
			</div>
		</div>
	</div>
</div>
<?php 
}

function wpbeginner_numeric_posts_nav() {
 
	if( is_singular() )
			return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
			return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/** Add current page to the array */
	if ( $paged >= 1 )
			$links[] = $paged;

	/** Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
	}


	/** Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="pagination-button pb-active"' : ' class="pagination-button"';

			printf( '<div%s><a href="%s">%s</a></div>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

			if ( ! in_array( 2, $links ) )
					echo '<div class="pagination-button">…</div>';
	}

	/** Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="pagination-button pb-active"' : ' class="pagination-button"';
			printf( '<div%s><a href="%s">%s</a></div>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/** Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
			if ( ! in_array( $max - 1, $links ) )
					echo '<div>…</div>' . "\n";

			$class = $paged == $max ? ' class="pagination-button pb-active"' : ' class="pagination-button"';
			printf( '<div%s><a href="%s">%s</a></div>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

}


function format_property_description($string) {
	if ($string == 'bbq') :
		return 'BBQ';
	elseif ($string == 'satellite_tv') :
		return 'Satellite TV';
	else :
		return ucwords(str_replace('_', ' ', $string));
	endif;
}

function string_convert_space($string, $newCharacter) {
	return str_replace(' ', $newCharacter, $string);
}


//hook to add a meta box
add_action( 'add_meta_boxes', 'featured_service' );

function featured_service() {
    //create a custom meta box
    add_meta_box( 'twice-meta', 'Speciality Service?', 'feature_service_func', 'services', 'side', 'high' );
}

function feature_service_func( $post ) {

    //retrieve the meta data values if they exist
    $twice_speciality_service = get_post_meta( $post->ID, '_twice_meta_speciality', true );

    echo 'Select yes below to make video featured';
    ?>
    <select name="twice_speciality_service">
			<option value="no" <?php selected( $twice_speciality_service, 'no' ); ?>>No</option>
			<option value="yes" <?php selected( $twice_speciality_service, 'yes' ); ?>>Yes</option>
    </select>
    <?php
}

//hook to save the meta box data
add_action( 'save_post', 'feature_service_save' );
function feature_service_save( $post_ID ) {
		global $post;
		if ( $post ) {
			if( $post->post_type == "services" ) {
					if ( isset( $_POST ) ) {
							update_post_meta( $post_ID, '_twice_meta_speciality', strip_tags( $_POST['twice_speciality_service'] ) );
					}
			}

		}
}

function our_offers_link() {

	if (pll_current_language() == 'en') {
		$link = "/twice-lifestyle/our-packages";
	} else if (pll_current_language() == 'fr') {
		$link = "/fr/twice-lifestyle-fr/our-packages-fr/";
	}

	return $link; 
}

function become_member_link() {

	if (pll_current_language() == 'en') {
		$link = "/twice-lifestyle/become-a-member/";
	} else if (pll_current_language() == 'fr') {
		$link = "/fr/twice-lifestyle-fr/become-a-member-fr/";
	}

	return $link;
}

//returns the link in the right language
function lang_link($passed_link) {
	if (pll_current_language() == 'en') {
		return $passed_link;
	} else if (pll_current_language() == 'fr') {
		if ($passed_link == '/twice-estates/cannes') return "/fr/twice-estates/cannes-fr/";
		if ($passed_link == '/twice-estates/#property-request-info') return "/fr/twice-estates-fr/#property-request-info";
		if ($passed_link == '/contact') return "/fr/contact-fr";
		if ($passed_link == '/twice-lifestyle/become-a-member') return "/fr/twice-lifestyle-fr/become-a-member-fr";
		if ($passed_link == '/twice-events/how-we-guarantee-the-success-of-your-event') return "/fr/twice-events-fr/how-we-guarantee-the-success-of-your-event-fr";
	}
}

//Estates filter

add_action('wp_ajax_filterestates', 'twice_estates_filter');
add_action('wp_ajax_nopriv_filterestates', 'twice_estates_filter');

// filter
// function my_posts_where( $where ) {
	
// 	$where = str_replace("meta_key = 'property_$", "meta_key LIKE 'property_%", $where);

// 	return $where;
// }

// add_filter('posts_where', 'my_posts_where');

function twice_estates_filter() {

	$term = $_POST['term'];
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'		=> 'estates',
		'tax_query' => array(
			array(
					'taxonomy'  => 'estate_locations',
					'field'     => 'slug',
					'terms'     => $term,
			)
		)
	);

	if (isset($_POST['action']) && $_POST['action'] == 'filterestates') {
		$args['meta_query'] = array( 'relation'=>'AND' );
	}

	foreach($_POST as $option_key => $option_value) {
		if ($option_key == 'action' || $option_key == 'term' || $option_value == 'any') continue;

		if ($option_value == 'on') {
			$args['meta_query'][] = array(
				'key' => 'property_' . $option_key,
				'value' => 1,
				'compare' => 'LIKE'
			);
		} else {
			$args['meta_query'][] = array(
				'key' => 'property_' . $option_key,
				'value' => $_POST[$option_key],
				'compare' => 'LIKE'
			);			
		}
	}
	// if (isset($_POST['bedrooms']) && $_POST['bedrooms'] != 'any') {
	// 	$args['meta_query'][] = array(
	// 		'key' => 'property_bedrooms',
	// 		'value' => $_POST['bedrooms'],
	// 		'compare' => 'LIKE'
	// 	);
	// }

	// if (isset($_POST['bathrooms']) && $_POST['bathrooms'] != 'any') {
	// 	$args['meta_query'][] = array(
	// 		'key' => 'property_bathrooms',
	// 		'value' => $_POST['bathrooms'],
	// 		'compare' => 'LIKE'
	// 	);
	// }

	// if (isset($_POST['air_conditioning']) && $_POST['air_conditioning'] != 'any') {
	// 	$args['meta_query'][] = array(
	// 		'key' => 'property_air_conditioning',
	// 		'value' => 1,
	// 		'compare' => 'LIKE'
	// 	);
	// }

	// if (isset($_POST['terrace']) && $_POST['terrace'] != 'any') {
	// 	$args['meta_query'][] = array(
	// 		'key' => 'property_terrace',
	// 		'value' => 1,
	// 		'compare' => 'LIKE'
	// 	);
	// }

	// if (isset($_POST['swimming_pool']) && $_POST['swimming_pool'] != 'any') {
	// 	$args['meta_query'][] = array(
	// 		'key' => 'property_swimming_pool',
	// 		'value' => 1,
	// 		'compare' => 'LIKE'
	// 	);
	// }

	// if (isset($_POST['swimming_pool']) && $_POST['swimming_pool'] != 'any') {
	// 	$args['meta_query'][] = array(
	// 		'key' => 'property_swimming_pool',
	// 		'value' => 1,
	// 		'compare' => 'LIKE'
	// 	);
	// }






	$query = new WP_Query( $args );
	if( $query->have_posts() ) :
		while( $query->have_posts() ): $query->the_post();
		$estate = get_field('property');
			$images = get_field('images');  ?>
				<div class="column is-4 property-listing" data-rooms="<?php echo $estate['bedrooms']['value']; ?>">
					<div class="prop-listing-container custom-clearfix">
						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo $images[0]['url']; ?>" alt="<?php echo $images[0]['alt']; ?>" class="prop-listing-pic">
						</a>

						<div class="columns is-mobile">
							<div class="column is-narrow is-hidden-touch">
								<img src="/wp-content/uploads/Asset-2@2xna.png" class="listing-fde-icon">
							</div>
							<div class="column">
							<h2 class="lead-sub prop-listing-title"><a href="<?php  the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<div class="level is-mobile">
									<div class="level-left">
										<div class="level-item">
											<div>
												<p class="number-of-rooms"><?php echo $estate['bedrooms']['value']; ?> <?php pll_e('Rooms'); ?></p>
											</div>
										</div>
									</div>
									<div class="level-right">
										<div class="level-item">
											<div>
												<p class="link-to-prop"><a href="<?php the_permalink(); ?>"><?php pll_e('View Property'); ?> <i class="fas fa-chevron-right"></i></a></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>

		<?php 
	endwhile;
		wp_reset_postdata();
	else :
		echo '<h3 class="slide-caption-text txt-center" style="margin-top:20px; width: 100%;">No properties found</h3>';
		echo '<p class="txt-center" style="margin-top: 20px; width:100%;"> Please adjust your filter options to find properties.</p>';
	endif;
 
	die();
}

//Register Translations
//Single Line
pll_register_string('Twice', 'Language', 'twice_language');
pll_register_string('Twice', 'Home', 'twice_language');
pll_register_string('Twice', 'Contact', 'twice_language');
pll_register_string('Twice', 'Contact Form', 'twice_language');
pll_register_string('Twice', 'Follow Us', 'twice_language');
pll_register_string('Twice', 'More Info', 'twice_language');
pll_register_string('Twice', 'Privacy', 'twice_language');
pll_register_string('Twice', 'Client Testimonials', 'twice_language');
pll_register_string('Twice', 'Our Team', 'twice_language');
pll_register_string('Twice', 'Meet our team of experts', 'twice_language');
pll_register_string('Twice', 'Connect with Twice Agency', 'twice_language');
pll_register_string('Twice', 'Read the Blog', 'twice_language');
pll_register_string('Twice', 'Read on', 'twice_language');
pll_register_string('Twice', 'Careers', 'twice_language');
pll_register_string('Twice', 'Terms and Conditions', 'twice_language');
pll_register_string('Twice', 'Become a member', 'twice_language');
pll_register_string('Twice', 'Enquire Now', 'twice_language');
pll_register_string('Twice', 'Contact the Team', 'twice_language');
pll_register_string('Twice', 'Location', 'twice_language');
pll_register_string('Twice', 'Sea View', 'twice_language');
pll_register_string('Twice', 'Living Area', 'twice_language');
pll_register_string('Twice', 'Bedrooms', 'twice_language');
pll_register_string('Twice', 'Bathrooms', 'twice_language');
pll_register_string('Twice', 'Terrace', 'twice_language');
pll_register_string('Twice', 'Plot', 'twice_language');
pll_register_string('Twice', 'Swimming Pool', 'twice_language');
pll_register_string('Twice', 'Heated Swimming Pool', 'twice_language');
pll_register_string('Twice', 'Indoor Swimming Pool', 'twice_language');
pll_register_string('Twice', 'Air Conditioning', 'twice_language');
pll_register_string('Twice', 'Wifi', 'twice_language');
pll_register_string('Twice', 'Satellite TV', 'twice_language');
pll_register_string('Twice', 'Alarm', 'twice_language');
pll_register_string('Twice', 'Video Surveillance', 'twice_language');
pll_register_string('Twice', 'Outdoor Kitchen', 'twice_language');
pll_register_string('Twice', 'Outdoor Lounge', 'twice_language');
pll_register_string('Twice', 'BBQ', 'twice_language');
pll_register_string('Twice', 'House Design', 'twice_language');
pll_register_string('Twice', 'days', 'twice_language');
pll_register_string('Twice', 'week', 'twice_language');
pll_register_string('Twice', 'weeks', 'twice_language');
pll_register_string('Twice', 'month', 'twice_language');
pll_register_string('Twice', 'months', 'twice_language');
pll_register_string('Twice', 'View Prices', 'twice_language');
pll_register_string('Twice', 'Apply now', 'twice_language');
pll_register_string('Twice', 'See Terms & Conditions', 'twice_language');
pll_register_string('Twice', 'Contact us for more information', 'twice_language');
pll_register_string('Twice', 'Contact us', 'twice_language');
pll_register_string('Twice', 'Any requests', 'twice_language');
pll_register_string('Twice', 'Application Twice Lifestyle', 'twice_language');
pll_register_string('Twice', '12 Months Lifestyle Package Information', 'twice_language');
pll_register_string('Twice', 'Corporate Twice Lifestyle Packages', 'twice_language');
pll_register_string('Twice', 'Restaurant', 'twice_language');
pll_register_string('Twice', 'Beach Club', 'twice_language');
pll_register_string('Twice', 'Car Rental', 'twice_language');
pll_register_string('Twice', 'Transfer', 'twice_language');
pll_register_string('Twice', 'Aviation', 'twice_language');
pll_register_string('Twice', 'Concert', 'twice_language');
pll_register_string('Twice', 'Theatre', 'twice_language');
pll_register_string('Twice', 'Night Life', 'twice_language');
pll_register_string('Twice', 'Chauffeur', 'twice_language');
pll_register_string('Twice', 'Exclusive Events', 'twice_language');
pll_register_string('Twice', 'Close Protection', 'twice_language');
pll_register_string('Twice', 'Sport', 'twice_language');
pll_register_string('Twice', 'Musical', 'twice_language');
pll_register_string('Twice', 'Other', 'twice_language');
pll_register_string('Twice', 'Accommodation', 'twice_language');
pll_register_string('Twice', 'View More', 'twice_language');
pll_register_string('Twice', 'Our Packages', 'twice_language');
pll_register_string('Twice', 'Our Speciality', 'twice_language');
pll_register_string('Twice', 'More Luxury Services', 'twice_language');
pll_register_string('Twice', 'Private Events', 'twice_language');
pll_register_string('Twice', 'Corporate Events', 'twice_language');
pll_register_string('Twice', 'How we manage your event', 'twice_language');
pll_register_string('Twice', 'For our properties in', 'twice_language');
pll_register_string('Twice', 'Portfolio', 'twice_language');
pll_register_string('Twice', 'Properties', 'twice_language');
pll_register_string('Twice', 'View Portfolio', 'twice_language');
pll_register_string('Twice', 'View Property', 'twice_language');
pll_register_string('Twice', 'View All', 'twice_language');
pll_register_string('Twice', 'Rooms', 'twice_language');
pll_register_string('Twice', 'Information Form', 'twice_language');
pll_register_string('Twice', 'Contact us for your event brief', 'twice_language');
pll_register_string('Twice', 'Lifestyle Services', 'twice_language');
pll_register_string('Twice', 'Our Packages', 'twice_language');

//Multi Line
pll_register_string('Twice', 'Our website is currently being updated, please come back later to discover our new content.', 'twice_language', true);
pll_register_string('Twice', 'Discover our packages for Individuals and Business', 'twice_language', true);
pll_register_string('Twice', 'Discover how we select our valued Members', 'twice_language', true);

// function twice_asp_products_rewrite( $args, $post_type ) {
// 	if ( $post_type == "asp-products" ) {
// 			$args['rewrite'] = array(
// 					'slug' => 'payments',
// 			);
// 	}

// 	return $args;
// }
// add_filter( 'register_post_type_args', 'twice_asp_products_rewrite', 20, 2 );
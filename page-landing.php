<div class="temp-warning"><?php pll_e('Our website is currently being updated, please come back later to discover our new content.') ?></div>
<?php
/* Template Name: Landing Page */

get_header();

global $phone;
?>

<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="landing__bg" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">

				<div class="columns landing__content">
					<div class="column is-centered-mobile">

						<h1 class="landing__title"><?php the_title(); ?></h1>

						<div class="landing__text">
							<?php the_content(); ?>
						</div>

					</div>

				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Twice_Agency
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">





<section class="section my-3">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5 txt--uc"><?php pll_e('Our Speciality'); ?></h1>
				</div>


				<div class="intro__sep">
					<?php echo twice_sep(); ?>
				</div>


			</div>
		</div>
	</div>
</section>

<?php 
	$args = array(
		'post_type' => 'services',
		'posts_per_page' => '-1',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'meta_query' => array(
			array(
				'key'     => '_twice_meta_speciality',
				'value'   => 'yes',
			),
		),
	);

	$loop = new WP_Query( $args );
	if ($loop->have_posts() > 0) :
?>
<section class="lifestyle-services py-5">
	<div class="container">
		<div class="columns is-multiline">
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="column is-4">
				<a href="<?php the_permalink();?>" >
					<?php echo get_the_post_thumbnail(get_the_id(), 'full', array('class' => 'services__image')); ?>
				</a>
				<div>
					<img src="/wp-content/uploads/Asset-2@2xna.png" alt="" class="listing-fde-icon-small">
					<div class="listing-info">
						<h2 class="lead-sub lifestyle-item-title"><a href="<?php the_permalink();?>" ><?php echo the_title(); ?></a></h2>

					</div>
				</div>
			</div>
		<?php endwhile; ?>
		</div>
	</div>
</section>

<?php endif; ?>





<section class="section my-3">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5 txt--uc"><?php pll_e('More Luxury Services'); ?></h1>
				</div>


				<div class="intro__sep">
					<?php echo twice_sep(); ?>
				</div>


			</div>
		</div>
	</div>
</section>









<?php 
	$args = array(
		'post_type' => 'services',
		'posts_per_page' => '-1',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'meta_query' => array(
			array(
				'key'     => '_twice_meta_speciality',
				'value'   => 'no',
			),
		),
	);

	$loop = new WP_Query( $args );
	if ($loop->have_posts() > 0) :
?>
<section class="lifestyle-services py-5">
	<div class="container">
		<div class="columns is-multiline">
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="column is-4">
				<a href="<?php the_permalink();?>" >
					<?php echo get_the_post_thumbnail(get_the_id(), 'full', array('class' => 'services__image')); ?>
				</a>
				<div class="is-clearfix lifestyle-services-title">
					<img src="/wp-content/uploads/Asset-2@2xna.png" alt="" class="listing-fde-icon-small">
					<div class="listing-info">
					<h2 class="lead-sub lifestyle-item-title"><a href="<?php the_permalink();?>" ><?php echo the_title(); ?></a></h2>

					</div>
				</div>
			</div>
		<?php endwhile; ?>
		</div>
	</div>
</section>

<?php endif; ?>





<section class="ls-cta my-5">
	<div class="container">
		<div class="columns">
			<div class="column has-text-centered">
				<div class="bg-light  py-5">
					<h2 class="lead-sub mb-3 txt--uc"><?php pll_e('Our Packages'); ?></h2>
					<p class="mb-3"><strong><?php pll_e('Discover our packages for Individuals and Business'); ?></strong></p>
					<a href="<?php echo our_offers_link(); ?>" class="d-ib btn btn--large btn--orange btn--hover-brown"><?php pll_e('View More'); ?></a>
				</div>
			</div>
			<div class="column has-text-centered">
				<div class="bg-light  py-5">
					<h2 class="lead-sub mb-3 txt--uc"><?php pll_e('Become a member'); ?></h2>
					<p class="mb-3 "><strong><?php pll_e('Discover how we select our valued Members') ?></strong></p>
					<a href="<?php echo become_member_link(); ?>" class=" d-ib  btn btn--large btn--orange btn--hover-brown"><?php pll_e('View More'); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>











		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

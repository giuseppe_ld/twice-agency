<?php
/**
 * Template Name: Events Homepage
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<!-- Hero Section -->
		<?php 
			$args = array(
				'post_type' => 'events',
				'posts_per_page' => '-1',
				'orderby' => 'menu_order',
				'order' => 'ASC',
				
			);

			$loop = new WP_Query( $args );
			if ($loop->have_posts() > 0) : ?>
			<div class="hero-slider">
				<div class="hero-slider-inner owl-carousel">
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php $term = wp_get_post_terms( $post->ID, 'event_type'); ?>
					<?php if (get_field('alt_hero')) : 
						$slide = get_field('alt_hero'); ?>
					
						<div class="hero-slide" style="background-image: url('<?php echo $slide['url']; ?>');">
								<div class="slide-caption">
									<p class="slide-caption-text txt--uc"><?php the_title(); ?></p>
								</div>
						</div>
					<?php else : ?>
						<div class="hero-slide" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_id(), 'full'); ?>');">
								<div class="slide-caption">
									<p class="slide-caption-text txt--uc"><?php the_title(); ?></p>
								</div>
						</div>

					<?php endif; ?>
				<?php endwhile; ?>
				</div>

			</div>
			<?php wp_reset_postdata(); ?>
		<?php wp_reset_query(); ?>
			
		<?php endif; ?>
			<!-- Hero Section End-->

<section class="section my-3 ">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5"><?php the_title(); ?></h1>
				</div>


				<div class="intro__sep">
					<?php echo twice_sep(); ?>
				</div>

				<div class="readable-type-sub mb-3">
				<h2 class="lead-sub txt--uc"><?php the_field('quote'); ?></h2>
				</div>

				<div class="readable-type-small">
					<?php the_field('content') ?>
				</div>

			</div>
		</div>
	</div>
</section>



<section class="events-homepage-properties py-5">
	<div class="container">
		<div class="columns">

		<?php
   $args = array(
               'taxonomy' => 'event_type',
               'orderby' => 'name',
							 'order'   => 'DSC'
           );

   $cats = get_categories($args);

	 foreach($cats as $cat) : ?>
			<div class="column">
				<a href="<?php echo get_term_link($cat->term_id) ?>" title="<?php pll_e($cat->name); ?>">
					<div class="<?php echo $cat->slug; ?>-event bg-dark py-5 p-rel">
						<div class="event-type-inner">
							<h2 class="txt-center lead-sub txt--uc"><?php pll_e($cat->name); ?></h2>
							<div class="event-type-list">
								<?php
								$args = array(
										'post_type' => 'events',
										'numberposts' => '-1',
										'orderby' => 'menu_order',
										'order' => 'ASC',
										'tax_query' => array(
												array(
														'taxonomy' => 'event_type',
														'field' => 'slug',
														'terms' => $cat->slug,
												)
										),
								); 

								$events = get_posts($args);

								foreach ($events as $event) : ?>
									<p class="txt-center m-0"><?php echo $event->post_title; ?></p>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</a>
			</div>
		<?php endforeach; ?>
		<?php wp_reset_postdata(); ?>
		<?php wp_reset_query(); ?>
			
		</div>
	</div>
</section>


<!-- 



<section class="  py-5 ">
	<div class="container">
		<div class="columns">
			<div class="column is-centered has-text-centered">
				<h2 class="lead-sub txt-center ">FEATURED IN</h2>
				<div class="intro__sep mb-5">
					<?php echo twice_sep_small(); ?>
				</div>
				<div class="columns">
					<div class="column">
						<img src="/wp-content/uploads/Untitled-6.jpg" alt="">
					</div>
				</div>

			</div>
		</div>
	</div>
</section> -->



<section class=" mt-5 py-5 bg-light">
	<div class="container">
		<div class="columns">
			<div class="column is-centered has-text-centered">
				<h2 class="lead-sub txt-center mb-3 mt-3 txt--uc"><?php pll_e('How we manage your event'); ?></h2>
				<div class="intro__sep mb-3">
					<?php echo twice_sep_small(); ?>
				</div>

				<div class="columns  is-mobile is-multiline">

					<?php 
					$processes = get_field('event_process');
					foreach ($processes as $process_key => $process_val) : ?>
					
						<?php if ($process_key == 0) : ?>
							<div class="column is-half-mobile">
								<p class="txt-center mb-3"><?php echo $process_key + 1 ?></p>
								<div class="epf-item p-rel">
									<img src="<?php echo $process_val['icon']['url']; ?>" class="epf-icon-1-pos-adj">
								</div>
								<p class="txt-center mb-3 mt-3"><strong><?php echo $process_val['title']; ?></strong></p>
							</div>
						<?php else : ?>
							<div class="column is-half-mobile">
								<p class="txt-center mb-3"><?php echo $process_key + 1 ?></p>
								<div class="epf-item p-rel">
								<div class="icon-sep-line"></div>
									<img src="<?php echo $process_val['icon']['url']; ?>">
								</div>
								<p class="txt-center mb-3 mt-3"><strong><?php echo $process_val['title']; ?></strong></p>
							</div>
						<?php endif; ?>
					<?php endforeach;	?>

				</div>

				<a href="<?php echo lang_link('/twice-events/how-we-guarantee-the-success-of-your-event'); ?>" class=" d-ib btn btn--large btn--orange btn--hover-brown"><?php pll_e('View More'); ?></a>
			</div>
		</div>
	</div>
</section>








		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

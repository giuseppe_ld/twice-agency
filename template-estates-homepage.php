<?php
/**
 * Template Name: Estates Homepage
 */

get_header();

$slides = get_field('slideshow');

if (pll_current_language() == 'fr') :
	$portfolio_link = "/fr/twice-estates/tous/";
else :
	$portfolio_link = "/twice-estates/all";
endif;

?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<!-- Hero Section -->
			<div class=" hero-slider">
				<div class="hero-slider-inner owl-carousel">

				<?php foreach ( $slides as $slide ) :
				
				?>
				  <div class="hero-slide" style="background-image: url(<?php echo $slide['url']; ?>);">
							<a href="<?php echo $portfolio_link; ?>" class="hero-btn p-abs d-ib btn btn--large btn--orange btn--hover-brown"><?php pll_e('View Portfolio'); ?></a>
							<div class="slide-caption">
							<p class="slide-caption-text"><?php echo $slide['description']; ?></p>
							</div>
					</div>
				<?php endforeach; ?>
				</div>

			</div>

			<!-- Hero Section End-->



<section class="section my-3">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5">TWICE ESTATES</h1>
				</div>


				<div class="intro__sep">
					<?php twice_sep(); ?>
				</div>

				<div class="readable-type-sub mb-3">
					<h2 class="lead-sub txt--uc"><?php the_field('quote'); ?></h2>
				</div>

				<div class="readable-type-small mb-3">
					<?php the_field('content') ?>
				</div>

			</div>
		</div>
	</div>
</section>


<a name="property-request-info" style="position:relative; top: -150px; display:block;"></a>
<section class="estate-homepage-form bg-light py-5 mt-5">
	<div class="container">
		<div class="columns ">
			<div class="column has-text-centered">
			<?php $form = get_field('form'); ?>
			<h3 class="lead-sub txt--uc"><?php echo $form['title']; ?></h3>

			<div class="intro__sep">
				<?php twice_sep(); ?>
			</div>				
			</div>
		</div>

		<?php echo do_shortcode($form['shortcode']); ?>
	</div>
</section>



<section class="estate-homepage-properties py-5">
	<div class="container">
		<div class="columns">


			<div class="column mb-3">
				<p class="txt-center tct--uc"><strong><?php pll_e('For our properties in'); ?> CANNES</strong></p>
				<div class="estate-location location-cannes bg-dark py-5 p-rel">
					<a href="<?php echo lang_link('/twice-estates/cannes'); ?>" class="ehp-btn p-abs d-ib btn btn--large btn--orange btn--hover-brown"><?php pll_e('Portfolio'); ?></a>
				</div>
			</div>

			<div class="column mb-3">
				<p class="txt-center tct--uc"><strong><?php pll_e('For our properties in'); ?> MONACO</strong></p>
				<div class="estate-location location-monaco bg-dark py-5 p-rel">
					<a href="<?php echo lang_link('/twice-estates/#property-request-info'); ?>" class="ehp-btn p-abs d-ib btn btn--large btn--orange btn--hover-brown"><?php pll_e('Contact us'); ?></a>
				</div>
			</div>

			<div class="column mb-3">
				<p class="txt-center tct--uc"><strong><?php pll_e('For our properties in'); ?> SAINT TROPEZ</strong></p>
				<div class="estate-location location-st-tropez bg-dark py-5 p-rel">
					<a href="<?php echo lang_link('/twice-estates/#property-request-info'); ?>" class="ehp-btn p-abs d-ib btn btn--large btn--orange btn--hover-brown"><?php pll_e('Contact us'); ?></a>
				</div>
			</div>

		</div>
	</div>
</section>



		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Twice_Agency
 */

get_header();
$property_info = get_field('property');
$images = get_field('images');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">




<section class="section my-3">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5" id="villa-name"><?php the_title(); ?></h1>
				</div>


				<div class="intro__sep mb-5">
					<?php echo twice_sep(); ?>
				</div>


			</div>
		</div>
	</div>
</section>







<section class="single-estate-gallery pb-5">
	<div class="container narrow-container">
		<div class="columns is-multiline">
			<?php
			foreach ( $images as $image_key => $image_val) :

				if ($image_key == 0) : ?>
					<div class="column is-12">
						<img src="<?php echo $image_val['url']; ?>" <?php if ($image_val['alt']) echo 'alt="' . $image_val['alt'] . '"'; ?> style="width:100%" class="full-width-image" onclick="openModal();currentSlide(<?php echo $image_key + 1; ?>)"></a>
					</div>
				<?php else : ?>
					<div class="column is-4">
					<img src="<?php echo $image_val['url']; ?>" <?php if ($image_val['alt']) echo 'alt="' . $image_val['alt'] . '"'; ?> style="width:100%" class="full-width-image" onclick="openModal();currentSlide(<?php echo $image_key + 1; ?>)"></a>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>



		</div>
	</div>
</section>



<section class="single-property-description bg-light py-5">
	<div class="container narrow-container">
		<div class="columns is-multiline" style="justify-content: space-evenly;">
		<div class="column is-full has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-3">Description</h1>
				</div>


				<div class="intro__sep mb-3">
					<?php echo twice_sep(); ?>
				</div>


			</div>

			<div class="column is-11">
				<div class="tick-list-inner">
					<div class="gray-ticks">
						<?php foreach ($property_info as $info => $info_value) : ?>
							<?php if ($info_value) : ?>
								<div>
									<i class="fas fa-check"></i><strong><?php pll_e(format_property_description($info)); ?></strong>
									<?php
									 if (is_array($info_value)) : echo ': ' . $info_value['value']; else : echo ($info_value != 1) ? ': <span id="info-' . $info . '">' . $info_value . '</span>' : '';  endif;?>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>

					</div>

				</div>
			</div>


		</div>
	</div>
</section>




<section class="py-5">
	<div class="container">
		<div class="columns">
			<div class="column is-centered has-text-centered">
				<h2 class="txt-center bold-lead mb-4" id="request-price-title"><?php pll_e('Information Form'); ?></h2>
				<div id="property-request-form">
				<?php
				if (pll_current_language() == 'fr') :
					echo do_shortcode('[contact-form-7 id="5216" title="Twice Estates - Property Requirements FR"]');
				else :
					echo do_shortcode('[contact-form-7 id="4542" title="Twice Estates - Property Requirements"]');
				endif;
			?>				
				</div>
			</div>
		</div>
	</div>
</section>


<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">
	<?php foreach ( $images as $image_key => $image_val) : ?>
    <div class="mySlides">
      <img src="<?php echo $image_val['url']; ?>" style="width:100%">
    </div>
	<?php endforeach; ?>
    

    <!-- Next/previous controls -->
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <!-- Caption text -->
    <div class="caption-container">
      <p id="caption"></p>
    </div>

    <!-- Thumbnail image controls -->
		<div class="columns">
			<?php foreach ( $images as $image_key => $image_val) : ?>
				<div class="column">
					<img class="demo" src="<?php echo $image_val['url']; ?>" onclick="currentSlide(<?php echo $image_key + 1; ?>)" alt="<?php echo $image_val['alt']; ?>">
				</div>
				<?php endforeach; ?>

		</div>


  </div>
</div>


		</main><!-- #main -->
	</div><!-- #primary -->
	<script>
// Open the Modal
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

// Close the Modal
function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}

document.addEventListener('keydown', function(e) {
	if (e.keyCode === 37) {
		plusSlides(-1);
	} else if (e.keyCode === 39) {
		plusSlides(1);
	} else if (e.keyCode === 27) {
		closeModal();
	}
})

var requestButton = document.getElementById('request-property-info');

window.addEventListener('DOMContentLoaded', function(e) {
	villaName = document.getElementById('villa-name').innerHTML;
	villaLocation = document.getElementById('info-location').innerHTML;
	

	document.querySelector('[name="villa-name"]').value = villaName;
	document.querySelector('[name="location"]').value = villaLocation;

})
</script>
<?php
get_footer();

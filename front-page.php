<?php
get_header();

if (pll_current_language() == 'fr') :
	$services = get_field('services_fr', 'option');
else :
		$services = get_field('services', 'option');
endif;
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<section class="section service content-space--bottom">
			<div class="columns is-multiline is-centered">
				<?php foreach ($services as $service) : ?>
					<?php $post = get_post($service); ?>
					<div class="column is-4-desktop is-6-tablet">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<div class="service__box service__<?php echo $post->post_name; ?>" style="background-image:url(<?php echo the_post_thumbnail_url($post->ID, 'post-thumbnail'); ?>)">

								<div class="service__title">
									<?php the_title(); ?>
								</div>
							</div>
						</a>
					</div>
				<?php endforeach; ?>
				<?php wp_reset_query(); ?>
			</div>
		</section>

		<section class="section intro content-space--bottom">
			<div class="container">
				<div class="columns is-centered">
					<div class="column has-text-centered">
						<div class="intro__title">
							<h1><?php single_post_title(); ?></h1>
						</div>

						<div class="intro__sep">
							<?php echo twice_sep(); ?>
						</div>

						<div class="intro__content">
							<?php the_content(); ?>
						</div>

					</div>
				</div>
			</div>
		</section>


		<section class="section testimonials">
			<?php get_template_part( 'template-parts/section', 'testimonials' ); ?>
		</section>

		<section class="section team">
			<?php get_template_part( 'template-parts/section', 'team' ); ?>
		</section>

		<section class="section connect">
			<?php get_template_part( 'template-parts/section', 'connect' ); ?>
		</section>


		<section id="contact" class="section contact">
			<?php get_template_part( 'template-parts/section', 'contact' ); ?>
		</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var babel = require('gulp-babel');
var browserSync = require('browser-sync').create();


gulp.task('sass', function() {
	return gulp.src('sass/**/*.scss')
		.pipe(plumber())
		.pipe(sass.sync({outputStyle: 'expanded'}))
		.pipe(gulp.dest(''))
		.pipe(browserSync.stream());
});

gulp.task('js', function() {
	return gulp.src('src/**/*.js')
		.pipe(babel())
		.pipe(gulp.dest("js"))
		.pipe(browserSync.stream());

})

gulp.task('watch', ['sass', 'js'], function() {
	browserSync.init({
		proxy: "http://twice.local"
	});
	gulp.watch('src/*.js', ['js']);
	gulp.watch('sass/**/*.scss', ['sass']);
	gulp.watch('js/**/*.js').on('change', browserSync.reload);
	gulp.watch('./**/*.php').on('change', browserSync.reload);
});




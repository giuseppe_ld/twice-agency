<?php
/**
 * Template Name: Our Offers
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Twice_Agency
 */

get_header();

$short_term = get_field('short_term');
$silver = $short_term['silver'];
$gold = $short_term['gold'];
$black = $short_term['black'];
$twelve_month = get_field('12_month');
$corporate = get_field('corporate');

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">





			<section class="section my-3">
				<div class="container ">
					<div class="columns is-centered">
						<div class="column has-text-centered">
							<div class="mb-3">
								<h1 class="lead-title pt-5"><?php the_field('page_heading'); ?></h1>
								<div class="offerspage-sub"><?php the_field('sub_heading') ?></div>
							</div>


							<div class="intro__sep mb-5">
								<?php echo twice_sep(); ?>
							</div>

							<div class="readable-type-sub mb-3">
								<div class="offerspage-sub"><strong><?php echo $short_term['title']; ?></strong></div>
								<div class="offerspage-sub"><?php echo $short_term['subheading']; ?></div>
							</div>

						</div>
					</div>
				</div>
			</section>





<section class="short-term-packages mt-5">
	<div class="container-offers">
		<div class="columns">
			<div class="column mb-10">
				<div class="offer-column-inner">
					<div class="offer-header-silver offer-silver txt-center txt--uc">
						<?php echo $silver['title'] ?>
					</div>
					<div class="offer-details">
						<h2 class="package-header txt-center txt--uc"><?php echo $silver['intro'] ?></h2>
						<div class="columns is-desktop has-text-center">
							<div class="column no-pb-m-small ">
									<ul class="package-list">
										<?php for ( $i = 0; $i < count($silver['options']); $i+=2 ) {
											echo '<li>' . pll__($silver['options'][$i]) . '</li>';
										} ?>
									</ul>

							</div>

							<div class="column no-pb-m-small b-l-light">
								<ul class="package-list">
								<?php for ( $i = 1; $i < count($silver['options']); $i+=2 ) {
											echo '<li>' . pll__($silver['options'][$i]) . '</li>';
										} ?>
								</ul>

							</div>
						</div>
						<a  class="btn btn--large btn--hover-brown offers-acc-btn accordion-offers1"><?php pll_e('View Prices'); ?></a>
						<div class="panel">
							<div class="columns has-text-centered pt-3">
								<div class="column is-4 has-text-centered">
										<h4 class="price-head">2 <?php pll_e('days'); ?></h4>
										<p class="price txt-center"><?php echo $silver['price']['2_days']; ?></p>
								</div>
								<div class="column is-4 has-text-centered">
									<h4 class="price-head">2 <?php pll_e('weeks'); ?></h4>
									<p class="price txt-center"><?php echo $silver['price']['2_weeks']; ?></p>

								</div>
								<div class="column is-4 has-text-centered">
									<h4 class="price-head">1 <?php pll_e('month'); ?></h4>
									<p class="price txt-center"><?php echo $silver['price']['1_month']; ?></p>

								</div>
							</div>
						</div>
					</div>
				</div>


			</div> <!-- .silver -->
			<div class="column mb-10">
				<div class="offer-column-inner">
					<div class="offer-header-gold offer-gold txt-center txt--uc">
						<?php echo $gold['title'] ?>
					</div>
					<div class="offer-details">
						<h2 class="package-header txt-center txt--uc"><?php echo $gold['intro'] ?></h2>
						<div class="columns is-desktop has-text-center">
							<div class="column no-pb-m-small ">
									<ul class="package-list">
										<?php for ( $i = 0; $i < count($gold['options']); $i+=2 ) {
											echo '<li>' . pll__($gold['options'][$i]) . '</li>';
										} ?>
									</ul>

							</div>

							<div class="column no-pb-m-small b-l-light">
								<ul class="package-list">
								<?php for ( $i = 1; $i < count($gold['options']); $i+=2 ) {
											echo '<li>' . pll__($gold['options'][$i]) . '</li>';
										} ?>
								</ul>

							</div>
						</div>
						<a  class="btn btn--large btn--hover-brown offers-acc-btn accordion-offers1"><?php pll_e('View Prices'); ?></a>
						<div class="panel">
							<div class="columns has-text-centered pt-3">
								<div class="column is-4 has-text-centered">
										<h4 class="price-head">2 <?php pll_e('days'); ?></h4>
										<p class="price txt-center"><?php echo $gold['price']['2_days']; ?></p>
								</div>
								<div class="column is-4 has-text-centered">
									<h4 class="price-head">2 <?php pll_e('weeks'); ?></h4>
									<p class="price txt-center"><?php echo $gold['price']['2_weeks']; ?></p>

								</div>
								<div class="column is-4 has-text-centered">
									<h4 class="price-head">1 <?php pll_e('month'); ?></h4>
									<p class="price txt-center"><?php echo $gold['price']['1_month']; ?></p>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- .gold -->

			<div class="column mb-10">
				<div class="offer-column-inner">
					<div class="offer-header-black offer-black txt-center txt--uc">
						<?php echo $black['title'] ?>
					</div>
					<div class="offer-details">
						<h2 class="package-header txt-center txt--uc"><?php echo $black['intro'] ?></h2>
						<div class="columns is-desktop has-text-center">
							<div class="column no-pb-m-small ">
									<ul class="package-list">
										<?php for ( $i = 0; $i < count($black['options']); $i+=2 ) {
											echo '<li>' . pll__($black['options'][$i]) . '</li>';
										} ?>
									</ul>

							</div>

							<div class="column no-pb-m-small b-l-light">
								<ul class="package-list">
								<?php for ( $i = 1; $i < count($black['options']); $i+=2 ) {
											echo '<li>' . pll__($black['options'][$i]) . '</li>';
										} ?>
								</ul>

							</div>
						</div>
						<a  class="btn btn--large btn--hover-brown offers-acc-btn accordion-offers1"><?php pll_e('View Prices'); ?></a>
						<div class="panel">
							<div class="columns has-text-centered pt-3">
								<div class="column is-4 has-text-centered">
										<h4 class="price-head">2 <?php pll_e('days'); ?></h4>
										<p class="price txt-center"><?php echo $black['price']['2_days']; ?></p>
								</div>
								<div class="column is-4 has-text-centered">
									<h4 class="price-head">2 <?php pll_e('weeks'); ?></h4>
									<p class="price txt-center"><?php echo $black['price']['2_weeks']; ?></p>

								</div>
								<div class="column is-4 has-text-centered">
									<h4 class="price-head">1 <?php pll_e('month'); ?></h4>
									<p class="price txt-center"><?php echo $black['price']['1_month']; ?></p>

								</div>
							</div>
						</div>
					</div>
				</div>


			</div><!-- .black -->
		</div>
		
			<div class="st-cta">
					<a href="mailto:membership@twice-agency.com?subject=<?php pll_e('Application Twice Lifestyle'); ?>" class=" btn st-cta-btn btn--large btn--orange btn--hover-brown"><?php pll_e('Apply now'); ?></a>
				<p class="txt-center offers-tnc"><?php pll_e('See Terms & Conditions'); ?><br>
				<?php pll_e('Contact us for more information'); ?></p>
			</div>

	</div>

</section>


<section class="section my-5">
    <div class="container ">
        <div class="columns is-centered">
            <div class="column has-text-centered">
                <hr class="twice-hr--orange">
            </div>
        </div>
    </div>
</section>


<section class="section my-5">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">

				<div class="readable-type-sub mb-3">
					<div class="offerspage-sub"><strong><?php echo $twelve_month['title']; ?></strong></div>
					<div class="offerspage-sub"><?php echo $twelve_month['subheading']; ?></div>
				</div>

			</div>
		</div>
	</div>
</section>



<section class="short-term-packages mt-5">
	<div class="container-12m">
		<div class="columns">
			<div class="column  mb-10">
				<div class="offer-column-inner">
					<div class="offer-header-black offer-black txt-center txt--uc">
						<?php echo $twelve_month['black']['title']; ?>
					</div>
					<div class="offer-details">
						<h2 class="package-header txt-center txt--uc"><?php pll_e('Any requests'); ?></h2>
						<div class="columns is-desktop has-text-center">
							<div class="column no-pb-m-small ">
									<ul class="package-list">
										<?php for ( $i = 0; $i < 3; $i++ ) {
											echo '<li>' . pll__($twelve_month['black']['options'][$i]) . '</li>';
										} ?>
									</ul>

							</div>

							<div class="column no-pb-m-small b-l-light">
								<ul class="package-list">
									<?php for ( $i = 3; $i < 6; $i++ ) {
										echo '<li>' . pll__($twelve_month['black']['options'][$i]) . '</li>';
									} ?>
								</ul>

							</div>

							<div class="column no-pb-m-small b-l-light ">
									<ul class="package-list">
										<?php for ( $i = 6; $i < 9; $i++ ) {
											echo '<li>' . pll__($twelve_month['black']['options'][$i]) . '</li>';
										} ?>
									</ul>

							</div>

							<div class="column no-pb-m-small b-l-light">
								<ul class="package-list">
									<?php for ( $i = 9; $i < 12; $i++ ) {
										echo '<li>' . pll__($twelve_month['black']['options'][$i]) . '</li>';
									} ?>

								</ul>

							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

			<div class="st-cta">
					<a href="mailto:membership@twice-agency.com?subject=<?php pll_e('12 Months Lifestyle Package Information'); ?>" class=" btn st-cta-btn btn--large btn--orange btn--hover-brown"><?php pll_e('Contact us'); ?></a>
					<p class="txt-center offers-tnc"><?php pll_e('See Terms & Conditions'); ?><br>
				<?php pll_e('Contact us for more information'); ?></p>
			</div>

	</div>

</section>








	<section class="section my-3 bg-light mt-5 pb-5">
		<div class="container ">
			<div class="columns is-centered">
				<div class="column has-text-centered">
					<div class="mb-3">
						<h1 class="lead-title pt-5"><?php echo $corporate['title']; ?></h1>
						<p class="offerspage-sub txt-center"><?php echo $corporate['subheading']; ?></p>
					</div>


					<div class="intro__sep">
						<?php echo twice_sep(); ?>
					</div>

					<div class="readable-type-sub mb-3">
						<div class="offerspage-sub">
							<?php echo $corporate['intro']; ?>
						</div>
					</div>

				</div>
			</div>
		</div>



		<div class="container-12m" style="margin-top: 50px;">
			<div class="columns">
				<div class="column  mb-10 ">
					<div class="offer-column-inner bg-white">
						<div class="offer-header-black offer-black-corp txt-center txt--uc">
							<?php echo $corporate['black']['title']; ?>
						</div>
						<div class="offer-details">
							<h2 class="package-header txt-center txt--uc"><?php echo $corporate['black']['intro']; ?></h2>
							<div class="columns is-desktop has-text-center">

								<div class="column no-pb-m-small ">
										<ul class="package-list">
											<?php for ( $i = 0; $i < 3; $i++ ) {
												echo '<li>' . pll__($corporate['black']['options'][$i]) . '</li>';
											} ?>
										</ul>
								</div>

								<div class="column no-pb-m-small b-l-light">
									<ul class="package-list">
										<?php for ( $i = 3; $i < 6; $i++ ) {
											echo '<li>' . pll__($corporate['black']['options'][$i]) . '</li>';
										} ?>
									</ul>

								</div>

								<div class="column no-pb-m-small b-l-light ">
								<ul class="package-list">
										<?php for ( $i = 6; $i < 9; $i++ ) {
											echo '<li>' . pll__($corporate['black']['options'][$i]) . '</li>';
										} ?>
									</ul>
								</div>

								<div class="column no-pb-m-small b-l-light ">
								<ul class="package-list">
										<?php for ( $i = 9; $i < 12; $i++ ) {
											echo '<li>' . pll__($corporate['black']['options'][$i]) . '</li>';
										} ?>
									</ul>
								</div>
							</div>

							<div class="">
								<div class="columns has-text-centered pt-3">
									<div class="column  has-text-centered">
											<h4 class="price-head txt--uc">2 <?php pll_e('days'); ?></h4>
									</div>
									<div class="column  has-text-centered">
											<h4 class="price-head txt--uc">4 <?php pll_e('days'); ?></h4>
									</div>
									<div class="column  has-text-centered">
											<h4 class="price-head txt--uc">1 <?php pll_e('week'); ?></h4>
									</div>
									<div class="column  has-text-centered">
											<h4 class="price-head txt--uc">2 <?php pll_e('weeks'); ?></h4>
									</div>
									<div class="column  has-text-centered">
											<h4 class="price-head txt--uc">12 <?php pll_e('months'); ?></h4>
									</div>

								</div>
							</div>
						</div>

					</div>

				</div>
</div>
<div class="st-cta ">
		<a href="mailto:membership@twice-agency.com?subject=<?php pll_e('Corporate Twice Lifestyle Packages'); ?>" class=" btn st-cta-btn btn--large btn--orange btn--hover-brown"><?php pll_e('Contact us'); ?></a>
		<p class="txt-center offers-tnc"><?php pll_e('See Terms & Conditions'); ?><br>
				<?php pll_e('Contact us for more information'); ?></p>
</div>

	</section>












		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

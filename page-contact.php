<?php
/**
 * Template Name: Contact Page
 */
get_header();

?>

<div id="primary" class="content-area">
		<main id="main" class="site-main">
	
	<section class="section">
		<div class="container">
			<div class="columns is-centered has-text-centered">
				<div class="column is-full">
					<div class="intro__title mb-3">
						<h2><?php pll_e('Contact the Team'); ?></h2>
						<h4><?php pll_e('For more information'); ?></h4>
					</div>
					<div class="intro__sep mb-3">
						<?php echo twice_sep_small(); ?>
					</div>
			</div>
		</div>
	</section>
		

		<section class="section team">
			<?php get_template_part( 'template-parts/section', 'team' ); ?>
		</section>

		<section id="contact" class="section contact">
			<?php get_template_part( 'template-parts/section', 'contact' ); ?>
		</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

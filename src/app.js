(function($) {

	$(document).ready(function(){
		$(".homepage-testimonials").owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			autoplaySpeed: true,
			autoplayTimeout: 11000,
		});

		$(".homepage-team").owlCarousel({
			loop: true,
			nav: true,
			dots: false,
			slideBy: 'page',
			autoplay: true,
			autoplaySpeed: true,
			autoplayTimeout: 4000,
			responsive: {
				0 : {
					items: 1
				},
				700 : {
					items: 2
				},
				1000 : {
					items: 3
				}
			}
		});

		jQuery(document.documentElement).keyup(function (event) {    

			var owl = jQuery(".owl-carousel");
			// handle cursor keys
			if (event.keyCode == 37) {
				 // go left
				 owl.trigger('prev.owl');
			} else if (event.keyCode == 39) {
				 // go right
				 owl.trigger('next.owl');
			}
	
	});

	});
	
	var scrollToContact = function() {
		$([document.documentElement, document.body]).animate({
			scrollTop: $("#contact").offset().top - 200
	}, 1000);
	}

	$(".contact-scroll").click(function() {
		scrollToContact();
	});

	if (window.location.hash === '#contact') {
		scrollToContact();
	}


	$(".hero-slider-inner").owlCarousel({
		autoplay: true,
		autoplaySpeed: true,
		autoplayTimeout: 4000,
    center: true,
    items:1,
    loop: true,
		nav: true,
		navElement: 'div',
		navClass:['slider-prev','slider-next'],
		navText:["<img src='/wp-content/uploads/Asset-5@2x.png'>","<img src='/wp-content/uploads/Asset-6@2x.png'>"],
});

$(".sep-slider-inner").owlCarousel({
	autoplay: true,
	autoplayTimeout: 4000,
	autoplaySpeed: true,
	center: true,
	items:1,
	loop:true,
	nav:true,
	navElement: 'div',
	navClass:['slider-prev','slider-next'],
	navText:["<img src='/wp-content/uploads/Asset-5@2x.png'>","<img src='/wp-content/uploads/Asset-6@2x.png'>"],

});

})(jQuery);

var acc = document.getElementsByClassName("accordion-offers1");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
		this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

// OLD BEDROOM FILTER
// const portfolioFilter = document.querySelector('.portfolio-filters');
// if (portfolioFilter) {
// 	portfolioFilter.addEventListener('click', (e) => {
// 		if (e.target.tagName == 'A') { 
	
// 			if (e.target.dataset.rooms == 'all') {
// 				displayAllProperties();
// 			}
	
// 			e.preventDefault();
// 			const filter = {};
	
// 			const target = e.target;
	
// 			//Update filter menu
			
// 			updateFilterMenu(target);
	
// 			//store filter options to filter object
// 			filter.rooms = target.dataset.rooms;
	
// 			//Save the filter options to localStorage
// 			window.sessionStorage.setItem('state', JSON.stringify(filter));
			
// 			filterProperties();
// 		}
// 	})
// }

// if (window.sessionStorage.getItem('state') && [...document.body.classList].includes('tax-estate_locations')) {
// 	filterProperties();
// 	updateFilterMenu();
// }

// function updateFilterMenu(target = 'empty') {
// 	//Update filter menu
// 	document.querySelector('.active-filter').classList.remove('active-filter');

// 	if (target == 'empty') {
// 		const filter = getState();
// 		document.querySelector(`[data-rooms="${filter.rooms}"]`).classList.add('active-filter');
// 	} else if (![...target.classList].includes('active-filter')) {
// 		target.classList.add('active-filter');
// 	}
// }

// function getState() {
// 	return JSON.parse(window.sessionStorage.getItem('state'));
// }

// function filterProperties() {
// 	//Get filter from Session Storage
// 	const filter = getState();
	

// 	if (filter.rooms == 'all') {
// 		displayAllProperties();
// 		return;
// 	}

// 	//Select all Property listings that don't match filter.rooms
// 	const propertiesToHide = document.querySelectorAll(`.property-listing:not([data-rooms="${filter.rooms}"])`);
	
// 	//add fade-out css effect
// 	propertiesToHide.forEach(property => property.classList.add('property-hidden'));

// 	//if filter selection has changed and was previously hidden, now show
// 	const hiddenProperties = document.querySelectorAll(`.property-listing.property-hidden[data-rooms="${filter.rooms}"]`);
// 	hiddenProperties.forEach(property => property.classList.remove('property-hidden'));


// }

// function displayAllProperties() {
// 	const hiddenProperties = document.querySelectorAll(`.property-listing.property-hidden`);
// 	hiddenProperties.forEach(property => property.classList.remove('property-hidden'));

// }


// -----------------------
// FILTER

(function($) {

	$('#filter--open').on('click', function() {
		$('#filter__options').slideToggle();
		$(this).toggleClass('btn--active');
	})
	$('#filter').submit(function() {
		const filter = $('#filter');

		$.ajax({
			url: filter.attr('action'),
			data: filter.serialize(),
			type: filter.attr('method'),
			beforeSend: function(xhr) {
				filter.find('button').html('<img src="/wp-content/uploads/twice-loader.svg" style="height:10px;">');
			},
			success: function(data) {
				filter.find('button').text('Apply filter');
				$('#filter--open').removeClass('btn--active');
				$('#filter__options').slideUp();
				$('#filter__response').html(data);
			}
		});
		return false;
	});

})(jQuery);

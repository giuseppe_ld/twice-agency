<?php

get_header();
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 

if (pll_current_language() == 'en') : 
	$page_title = $term->name . ' Properties';
elseif (pll_current_language() == 'fr') : 
	if ($term->name == 'Tous') {
		$page_title = 'Toutes les Propriétés';
	} else {
		$page_title = 'Proprietes De ' . $term->name;
	}
endif; 
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main has-header-cta">

<section class="section my-3">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5 txt--uc"><?php echo $page_title; ?></h1>
				</div>


				<div class="intro__sep mb-5">
					<?php echo twice_sep(); ?>
				</div>


			</div>
		</div>
	</div>
</section>

<section class="portfolio-filters">
<div class="container">
	<div class="block">
	<!-- <nav class="level">
		<div class="level-left">
			<p class="level-item"><a data-rooms="all" class="active-filter"><?php //pll_e('View All'); ?></a></p>
			<p class="level-item"><a data-rooms="2">2 <?php //pll_e('Rooms'); ?></a></p>
			<p class="level-item"><a data-rooms="3">3 <?php //pll_e('Rooms'); ?></a></p>
			<p class="level-item"><a data-rooms="4">4 <?php //pll_e('Rooms'); ?></a></p>
			<p class="level-item"><a data-rooms="5">5 <?php //pll_e('Rooms'); ?></a></p>
			<p class="level-item"><a data-rooms="6">6 <?php //pll_e('Rooms'); ?></a></p>
			<p class="level-item"><a data-rooms="7">7 <?php //pll_e('Rooms'); ?></a></p>
			<p class="level-item"><a data-rooms="8">8+ <?php //pll_e('Rooms'); ?></a></p>

		</div>
	</nav> -->
	<button class="btn btn--orange btn--large" id="filter--open"><?php pll_e('Filter'); ?></button>


	<div id="filter__options">

						<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
							<div class="filter__fields">
								<?php 
								$fields = get_field_objects();
								$filter_options = $fields['property']['sub_fields'];

								foreach ($filter_options as $option) {
									if ($option['type'] === 'text') continue;

									if ($option['type'] === 'select') { ?>
										<div class="form__group">
											<label><?php echo pll__($option['label']); ?></label>
											<select name="<?php echo $option['name']; ?>">
												<option value="any">Any</option>
												<?php foreach ($option['choices'] as $choice_key => $choice_value) { ?>
												<option value="<?php echo $choice_key; ?>"><?php echo pll__($choice_key); ?></option>
												<?php } ?>
											</select>
										</div>
									<?php }
									
									if ($option['type'] === 'number') { ?>
										<div class="form__group">
											<label><?php echo pll__($option['label']); ?></label>
											<select name="<?php echo $option['name']; ?>">
												<option value="any">Any</option>
												<?php for ($i = 1; $i <= 10; $i++) { ?>
												<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
												<?php } ?>
											</select>
										</div>
									<?php }
									
									if ($option['type'] === 'true_false') { ?>
										<div class="form__group">
											<label>
												<input type="checkbox" value="1" name="<?php echo $option['name'] ?>" /> <?php echo pll__($option['label']); ?>
											</label>
										</div>
									<?php } 
								} ?>
							</div>
							
							<button type="submit" class="btn btn--orange btn--hover-brown"><?php pll_e('Apply filter'); ?></button>
							<input type="hidden" name="action" value="filterestates" />
							<input type="hidden" name="term" value="<?php echo $term->name; ?>" />
						</form>

	</div>

</div>
</div>

</section>



<?php 	
	if (have_posts()) :
?>
<section class="section estate-homepage-properties">
	<div class="container">
		<div class="columns is-multiline" id="filter__response">
			<?php while ( have_posts() ) : the_post();
			$estate = get_field('property');
			$images = get_field('images');
			?>
				<div class="column is-4 property-listing" data-rooms="<?php echo $estate['bedrooms']['value']; ?>">
					<div class="prop-listing-container custom-clearfix">
						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo $images[0]['url']; ?>" alt="<?php echo $images[0]['alt']; ?>" class="prop-listing-pic">
						</a>

						<div class="columns is-mobile">
							<div class="column is-narrow is-hidden-touch">
								<img src="/wp-content/uploads/Asset-2@2xna.png" class="listing-fde-icon">
							</div>
							<div class="column">
							<h2 class="lead-sub prop-listing-title"><a href="<?php  the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<div class="level is-mobile">
									<div class="level-left">
										<div class="level-item">
											<div>
												<p class="number-of-rooms"><?php echo $estate['bedrooms']['value']; ?> <?php pll_e('Rooms'); ?></p>
											</div>
										</div>
									</div>
									<div class="level-right">
										<div class="level-item">
											<div>
												<p class="link-to-prop"><a href="<?php the_permalink(); ?>"><?php pll_e('View Property'); ?> <i class="fas fa-chevron-right"></i></a></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			<?php endwhile; ?>


			
			
		</div>
	</div>
</section>

<section class="section port-pagination mb-5">
	<div class="columns is-centered">
		<div class="column has-text-centered">
			<div class="pagination-buttons">
				<?php wpbeginner_numeric_posts_nav(); ?>
			</div>
		</div>
	</div>
</section>

<?php wp_reset_query(); ?>
	<?php endif; ?>




		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

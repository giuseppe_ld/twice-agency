<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Twice_Agency
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">





<section class="section my-3">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5"><?php the_title(); ?></h1>
				</div>


				<div class="intro__sep">
					<?php echo twice_sep(); ?>
				</div>


			</div>
		</div>
	</div>
</section>

<section class="lifestyle-single-page py-5">
	<div class="container narrow-container">
		<div class="columns is-multiline">

			<div class="column is-12">
			<?php echo get_the_post_thumbnail(get_the_id(), 'full', array('class' => 'full-width-image service-image')); ?>
			</div>

		</div>
	</div>
</section>

<section class="single-lifestyle-description">
	<div class="container">
		<div class="columns has-text-centered">
			<div class="column readable-type-small has-text-centered">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>

	<section class="single-lifestyle-description" style="margin:2rem auto 3rem;">
	    <div class="container">
	        <div class="columns is-centered">
	            <div class="column is-4-tablet">
	                <div class="level">
	                    <div class="level-left">
	                        <div class="level-item">
	                            <div><a href="/twice-lifestyle/our-offers/" class="btn btn--large btn--orange btn--hover-brown"><?php pll_e('Our Packages'); ?></a></div>
	                        </div>
	                    </div>
	                    <div class="level-right">
	                        <div class="level-item">
	                            <div> <a href="/contact" class="btn btn--large btn--black-outline btn--hover-brown">CONTACT</a></div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

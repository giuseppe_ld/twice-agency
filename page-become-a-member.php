<?php
/**
 * Template Name: Become a Member
 */

get_header();

$trust_section = get_field('trust_section');
$how_section = get_field('how_section');

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<!-- Hero Section -->

			<div class="hero-slider">
				<div class="hero-slider-inner owl-carousel">
					<div class="hero-slide" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_id(), 'full'); ?>);">
						<div class="slide-caption">
							<p class="slide-caption-text txt--uc"><?php the_title(); ?></p>
						</div>
					</div>
				</div>
			</div>

			<!-- Hero Section End-->


<section class="reasons-to-trust  py-5 ">
	<div class="container">
		<div class="columns">
			<div class="column is-centered has-text-centered">
			<h2 class="lead-sub txt-center mb-3 txt--uc"><?php echo $trust_section['title']; ?></h2>
				<div class="intro__sep mb-5">
					<?php echo twice_sep_small(); ?>
				</div>
				<div class="columns is-multiline is-mobile">

				<?php foreach ( $trust_section['reasons'] as $reason) : ?>
					<div class="column has-text-centered is-half-mobile">
						<div class="reason-icon">
							<img src="<?php echo $reason['icon']['url']; ?>" alt="<?php $reason['icon']['alt']; ?>">
						</div>
						<p class="txt--uc"><strong><?php echo $reason['reason']; ?></strong></p>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</section>


<section class="section testimonials">
	<?php get_template_part( 'template-parts/section', 'testimonials' ); ?>
</section>







<section class="reasons-to-trust  py-5 ">
	<div class="container">
		<div class="columns">
			<div class="column is-centered has-text-centered">
				<h2 class="lead-sub txt-center mb-3 txt--uc"><?php echo $how_section['title']; ?></h2>
				<div class="intro__sep mb-5">
					<?php echo twice_sep_small(); ?>
				</div>
				<?php echo $how_section['intro']; ?>
				<div class="columns mt-5">

					<?php foreach ($how_section['hows'] as $how) : ?>
						<div class="column has-text-centered how-col ">
							<div class="become-a-member-icon">
							<img src="<?php echo $how['icon']['url']; ?>" alt="<?php $how['icon']['alt']; ?>">
							</div>
							<?php echo $how['how']; ?>
						</div>
					<?php endforeach; ?>
					

				</div>

			</div>
		</div>
		<a href="<?php echo our_offers_link(); ?>" class="d-ib xl-btn-center btn btn--large btn--orange btn--hover-brown"><?php pll_e('Apply now'); ?></a>
	</div>

</section>






		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

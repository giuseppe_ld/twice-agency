<?php
/**
 * Template Name: Lifestyle Homepage
 */

get_header();

$trust_section = get_field('trust_section');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<!-- Hero Section -->
			<?php 
			$slides = get_field('slideshow');

			if ( $slides ) :
			?>
			<div class="hero-slider">
				<div class="hero-slider-inner owl-carousel">
					<?php foreach ($slides as $slide) : ?>
						<div class="hero-slide" style="background-image: url('<?php echo $slide['url'] ?>');">
								<div class="slide-caption">
									<p class="slide-caption-text txt--uc"><?php pll_e($slide['description']); ?></p>
								</div>
						</div>
					<?php endforeach; ?>
				</div>

			</div>
		<?php endif; ?>
			<!-- Hero Section End-->
			


<section class="section my-3 ">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5">TWICE LIFESTYLE</h1>
				</div>


				<div class="intro__sep">
					<?php echo twice_sep(); ?>
				</div>

				<div class="readable-type-sub mb-3">
				<h2 class="lead-sub txt--uc"><?php the_field('quote'); ?></h2>
				</div>

				<div class="readable-type-small">
					<?php the_field('content') ?>
				</div>

			</div>
		</div>
	</div>
</section>







<section class="reasons-to-trust mt-5 py-5 bg-light">
	<div class="container">
		<div class="columns">
			<div class="column is-centered has-text-centered">
				<h2 class="lead-sub txt-center mb-3 txt--uc"><?php echo $trust_section['title']; ?></h2>
				<div class="intro__sep mb-5">
					<?php echo twice_sep_small(); ?>
				</div>
				<div class="columns is-multiline is-mobile">

				<?php foreach ( $trust_section['reasons'] as $reason) : ?>
					<div class="column has-text-centered is-half-mobile">
						<div class="reason-icon">
							<img src="<?php echo $reason['icon']['url']; ?>" alt="<?php $reason['icon']['alt']; ?>">
						</div>
						<p class="txt--uc"><strong><?php echo $reason['reason']; ?></strong></p>
					</div>
				<?php endforeach; ?>

				</div>

				<?php if (pll_current_language() == 'en') : ?>
					<a href="/twice-lifestyle/services" class="btn btn--large btn--orange btn--hover-brown"><?php pll_e('Lifestyle Services') ?></a>
				<?php elseif (pll_current_language() == 'fr') : ?>
					<a href="/fr/twice-lifestyle/services" class="btn btn--large btn--orange btn--hover-brown"><?php pll_e('Lifestyle Services') ?></a>

				<?php endif; ?>
			</div>
		</div>
	</div>
</section>


<section class="section testimonials">
	<?php get_template_part( 'template-parts/section', 'testimonials' ); ?>
</section>


<section class="ls-cta my-5">
	<div class="container">
		<div class="columns">
			<div class="column has-text-centered">
				<div class="bg-light  py-5">
					<h2 class="lead-sub mb-3 txt--uc"><?php pll_e('Our Packages'); ?></h2>
					<p class="mb-3"><strong><?php pll_e('Discover our packages for Individuals and Business'); ?></strong></p>
					<a href="<?php echo our_offers_link(); ?>" class="d-ib btn btn--large btn--orange btn--hover-brown"><?php pll_e('View More'); ?></a>
				</div>
			</div>
			<div class="column has-text-centered">
				<div class="bg-light  py-5">
					<h2 class="lead-sub mb-3 txt--uc"><?php pll_e('Become a member'); ?></h2>
					<p class="mb-3 "><strong><?php pll_e('Discover how we select our valued Members') ?></strong></p>
					<a href="<?php echo become_member_link(); ?>" class=" d-ib  btn btn--large btn--orange btn--hover-brown"><?php pll_e('View More'); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>



		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Twice_Agency
 */
$language = pll_current_language();
global $phone;
$phone = 'gc_tel_'.$language;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	 crossorigin="anonymous">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'twice-agency' ); ?></a>

	<header id="masthead" class="site-header">

		<div class="level topbar">
			<div class="level-left">
				<div class="level-item">
					<div>
						<a href="tel:<?php echo get_field($phone, 'option'); ?>">
							<i class="fas fa-phone icon--white"></i> <span class="topbar__info"><?php echo get_field($phone, 'option'); ?></span>
						</a>
					</div>
				</div>
				<div class="level-item">
					<div><i class="fas fa-envelope icon--white"></i> <a href="mailto:<?php echo get_field('gc_email', 'option'); ?>"><span class="topbar__info"><?php echo get_field('gc_email', 'option'); ?></span></a></div></div>
				</div>

			<div class="level-right">
				<div class="level-item">
					<div class="topbar__language">
						<?php
							pll_e('Language');
						?>
					</div>
					<div class="topbar_polylang-list">
					<?php if ( is_active_sidebar( 'select_language' ) ) : ?>
							<?php dynamic_sidebar( 'select_language' ); ?>
					<?php endif; ?>
					</div>
				</div>

			</div>
		</div>


		<div class="level header__nav">
			<div class="level-left">
				<div class="level-item">
					<div>
						<?php the_custom_logo(); ?>
						<div class="logo__text">Twice Agency</div>
					</div>
				</div>
			</div>
			<div class="level-right">
				<div class="level-item">
					<div>
						<nav id="site-navigation" class="main-navigation">
							<button class="menu-toggle btn btn--orange" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'twice-agency' ); ?></button>
							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'main-menu',
							) );
							?>
						</nav><!-- #site-navigation -->
					</div>
				</div>
			</div>
		</div>

		<?php
		if ( is_page( array('Twice Estates', 'Twice Events', )) || is_tax('estate_locations') || is_singular('estates') || is_tax('event_type') || $post->post_parent == '22' || $post->post_parent == '24' || $post->post_parent == '191' || $post->post_parent == '189'):
			enquire_cta(); 
		elseif ( is_page(array('Twice Lifestyle')) || $post->post_parent == '20' || $post->post_parent == '193'|| is_singular('services') || is_post_type_archive('services')):
			member_cta();
		endif;
		?>
	</header><!-- #masthead -->

	<div id="content" class="site-content">

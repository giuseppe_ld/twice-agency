<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Twice_Agency
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">





<section class="section my-3">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5"><?php the_title(); ?></h1>
				</div>


				<div class="intro__sep">
					<?php echo twice_sep(); ?>
				</div>


			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">
		<div class="columns">
			<div class="column txt-center">
				<?php the_content(); ?>
			
			</div>
		</div>
	</div>
</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

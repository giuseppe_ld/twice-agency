<?php
/**
 * Template Name: How We Work Page
 */

get_header();

$processes = get_field('process');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<!-- Hero Section -->

			<div class=" hero-slider">
				<div class="hero-slider-inner owl-carousel">
				  <div class="hero-slide" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_id(), 'full'); ?>);">
					</div>
				</div>
			</div>

			<!-- Hero Section End-->



<section class="section my-3 ">
	<div class="container ">
		<div class="columns is-centered">
			<div class="column is-8 has-text-centered">
				<div class="mb-3">
					<h1 class="lead-title pt-5 txt--uc"><?php the_title(); ?></h1>
				</div>


				<div class="intro__sep">
					<?php echo twice_sep(); ?>
				</div>


			</div>
		</div>
	</div>
</section>




<section class=" mt-5 py-5">
	<div class="container-hww p-rel">
		<div class="vert-sep"></div>
		<?php 
		foreach ($processes as $process_key => $process_val) :
		$direction = ($process_key&1) ? "left" : "right";
		
			if ($process_key < count($processes) - 1) :
			?>

			<div class="hww-cont p-rel">
				<div class="hww-<?php echo $direction; ?> custom-clearfix">
					<div class="hww-number-<?php echo $direction; ?> ">
						<div class="p-rel">
							<h2 class="hww-number-<?php echo $direction; ?>-pos"><?php echo $process_key + 1; ?></h2>
						</div>
					</div>
					<div class="hww-<?php echo $direction; ?>-icon-small-screens"></div>
					<div class="hww-<?php echo $direction; ?>-icon"><img src="<?php echo $process_val['icon']['url']; ?>" class="hww-icon hww-<?php echo $direction; ?>-icon-pos hww-icon-1"></div>
					<div class="hww-<?php echo $direction; ?>-txt">
						<h3><?php echo $process_val['title']; ?></h3>
						<?php echo $process_val['content']; ?>
					</div>
				</div>
			</div>
			<?php endif;  ?>
		<?php endforeach; ?>




	</div>
</section>

<?php 
$lastProcess = end($processes);
?>

<div class="reporting-sec mb-5">
<div class="container  ">
	<div class="columns is-centered">
		<div class="column has-text-centered">
			<div class="hww-number-6 ">
				<div class="p-rel">
					<h2 class="hww-number-6-pos"><?php echo count($processes); ?></h2>
				</div>
			</div>
			<div class="readable-type-sub mb-3">
				<img src="<?php echo $lastProcess['icon']['url']; ?>" class="hww-icon ">
				<h1 class="lead-sub"><?php echo $lastProcess['title'] ?></h1>
			</div>

			<div class="readable-type-small txt-narrow-mob txt-center">
				<?php echo $lastProcess['content'] ?>
			</div>

		<a href="mailto:events@twice-agency.com?subject=Twice%20Event%20Enquiry" class=" d-ib  btn btn--large btn--orange btn--hover-brown"><?php pll_e('Contact us for your event brief'); ?></a>
	</div>
</div>

</div>
</div>









		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php
/**
 * Template Name: Careers Page
 */

get_header();

?>

<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<!-- Hero Section -->

		<div class="hero-slider">
	<div class="hero-slider-inner owl-carousel">
		<div class="hero-slide" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_id(), 'full'); ?>);">
		</div>
	</div>
</div>

<!-- Hero Section End-->
	
	<section class="section py-5">
		<div class="container">
			<div class="columns is-centered has-text-centered">
				<div class="column is-full">
					<div class="column has-text-centered">
						<div class="mb-3">
							<h1 class="lead-title pt-5"><?php pll_e('Careers'); ?></h1>
						</div>


						<div class="intro__sep">
							<?php twice_sep(); ?>
						</div>

						<div class="readable-type-sub mb-3">
							<h2 class="lead-sub txt--uc"><?php the_field('intro'); ?></h2>
						</div>

						<div class="readable-type-small mb-3">
							<?php the_field('content') ?>
						</div>
				</div>
			</div>
		</div>
	</section>
		

		<section class="section bg-dark career__quote py-4">
			<?php $quote = get_field('quote'); ?>
			<div class="quote__content"><?php echo $quote['content']; ?></div>
			<div class="quote__source"><?php echo $quote['source']; ?></div>
		</section>

		<section id="contact" class="section contact careers-contact">
		<div class="container">
		<div class="columns is-centered">
			<div class="column is-6-tablet has-text-centered">
			<div class="intro__title">
					<h2><?php pll_e('Contact Form'); ?></h2>
				</div>

			<div class="intro__sep">
				<?php twice_sep(); ?>
			</div>				
			</div>
		</div>
		
		<?php 
		if (pll_current_language() == 'fr') :
			echo do_shortcode('[contact-form-7 id="5217" title="Careers FR"]');
		
		else :
			echo do_shortcode('[contact-form-7 id="4921" title="Careers EN"]');
		endif;
		?>
	</div>
</section>

		</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php
$args = array(
	'post_type' => 'teammembers',
	'numberposts' => '-1',
	'orderby' => 'menu_order',
	'order' => 'ASC',
);

$team = get_posts($args);
?>

<div class="container">
	<div class="columns is-centered has-text-centered">
		<div class="column is-full">
			<?php if (!is_page('contact')) : ?>
				<div class="intro__title">
					<h1><?php pll_e('Our Team'); ?></h1>
					<h4><?php pll_e('Meet our team of experts'); ?></h4>
				</div>
			<?php endif; ?>
			<div class="homepage-team owl-carousel owl-theme">
				<?php
				foreach ($team as $member) :
					$headshot = get_field('headshot', $member->ID);
					$job_title = get_field('job_title', $member->ID);
					$email = get_field('email', $member->ID);
				?>
					<div>
						<div class="team__headshot">
							<img src="<?php echo $headshot['url']; ?>" />
						</div>

						<div class="team__name">
							<?php echo $member->post_title; ?>
						</div>

						<div class="team__jobtitle">
							<?php echo $job_title; ?>
						</div>

						<div class="team__email">
							<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
						</div>
					</div>

				<?php
				endforeach;
				?>
			</div>
		</div>
	</div>
</div>

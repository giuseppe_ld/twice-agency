<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Twice_Agency
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


<section class="section py-5">
		<div class="container">
			<div class="columns is-centered has-text-centered">
				<div class="column is-full">
						<div class="mb-3">
							<h1 class="lead-title pt-5"><?php the_title(); ?></h1>
						</div>


						<div class="intro__sep">
							<?php twice_sep(); ?>
						</div>

			</div>
		</div>
	</section>


	<section class="section">
		<div class="container">
			<div class="columns is-centered">
				<div class="column is-10">
					<div class="entry-content">
						<?php
						the_content();

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'twice-agency' ),
							'after'  => '</div>',
						) );
						?>
					</div><!-- .entry-content -->
				</div>
			</div>
		</div>
	</section>
</article><!-- #post-<?php the_ID(); ?> -->

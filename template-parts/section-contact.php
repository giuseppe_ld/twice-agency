<a name="contact-twice" style="position:relative; top: -200px; display:block;"></a>
<div class="section">
	<div class="container">
		<div class="columns is-centered has-text-centered">
			<div class="column is-9">
				<div class="intro__title">
					<h1>Contact Twice Agency</h1>
					<h4><?php pll_e('For more information'); ?></h4>
				</div>

			<?php
				if (pll_current_language() == 'fr') :
					echo do_shortcode('[contact-form-7 id="265" title="Contact Form FR"]');
				else :
					echo do_shortcode('[contact-form-7 id="7" title="Contact form EN"]');
				endif;
			?>			
			</div>
		</div>
	</div>
</div>

<?php
wp_reset_query();
$social_platforms = get_terms( array(
	'taxonomy' => 'social_platform',
	'hide_empty' => false,
	'order' => 'Id',
) );
?>

<div class="section">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-10-desktop is-12-mobile">
				<h3 class="title title--small title--center title--case-u"><?php pll_e('Connect with Twice Agency');?></h3>
				<div class="columns is-centered is-multiline">

					<?php
					$args = array(
						'post_type' => 'post',
						'language' => '',
						'numberposts' => '1',
					);

					$blog_posts = get_posts($args);

					foreach ($blog_posts as $blog_post) :

					?>
						<div class="column is-3-desktop is-6-tablet">
							<div class="card">
								<a href="<?php the_permalink($blog_post->ID); ?>" title="	<?php echo $blog_post->post_title; ?>" target="_blank">
									<div class="card-image card-image--blog">
										<figure class="image is-4by3">
											<?php echo get_the_post_thumbnail($blog_post, 'medium'); ?>
										</figure>
									</div>
								</a>

								<div class="card-sep"></div>

								<div class="card-content">
									<div class="content">
										<a href="<?php the_permalink($blog_post->ID); ?>" title="	<?php echo $blog_post->post_title; ?>" target="_blank">
											<?php echo $blog_post->post_title; ?>
										</a>
									</div>

									<a class="card-link" href="<?php the_permalink($blog_post->ID); ?>" title="	<?php echo $blog_post->post_title; ?>" target="_blank"><?php pll_e('Read the Blog'); ?></a>
								</div>
							</div>

							<?php wp_reset_query(); ?>

						</div>
					<?php endforeach; ?>



				<?php
				foreach ($social_platforms as $social_platform) :
					$args = array(
						'post_type' => 'social_post',
						'numberposts' => '1',
						'tax_query' => array(
							array(
								'taxonomy' => 'social_platform',
								'field' => 'slug',
								'terms' => $social_platform->slug,
							)
						),
					);

					$connect_posts = get_posts($args);

					foreach ($connect_posts as $connect_post) :

				?>
						<div class="column is-3-desktop is-6-tablet">
							<div class="card">
								<a href="<?php echo get_field('link', $connect_post->ID); ?>" title="	<?php echo $blog_post->post_title; ?>" target="_blank">
								<div class="card-image card-image--<?php echo $social_platform->slug; ?>">
										<figure class="image is-4by3">
											<?php echo get_the_post_thumbnail($connect_post, 'medium'); ?>
										</figure>
									</div>
								</a>

								<div class="card-sep"></div>

								<div class="card-content">
									<div class="content">
										<a href="<?php echo get_field('link', $connect_post->ID); ?>" title="	<?php echo $blog_post->post_title; ?>" target="_blank">
											<?php echo $connect_post->post_title; ?>
										</a>

									</div>

									<a class="card-link" href="<?php echo get_field('link', $connect_post->ID); ?>" title="<?php echo $connect_post->post_title; ?>" target="_blank"><?php pll_e('Read on'); ?> <?php echo $social_platform->name; ?></a>
								</div>
							</div>
							<?php //get_title(); ?>

							<?php //wp_reset_query(); ?>

						</div>
						<?php endforeach; ?>
					<?php endforeach; ?>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>

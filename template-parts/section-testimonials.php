<?php
$args = array(
	'post_type' => 'testimonials',
	'numberposts' => '-1',
);

$testimonials = get_posts($args);
?>

<h3 class="title title--small title--center title--orange title--case-u"><?php pll_e('Client Testimonials'); ?></h3>

<div class="section">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-9-tablet is-7-desktop">
				<div class="homepage-testimonials owl-carousel owl-theme">
					<?php
					foreach ($testimonials as $testimonial) : ?>

						<div>
							<div class="testimonial__quote">
								<?php echo $testimonial->post_content; ?>
							</div>
							
							<div class="testimonial__source">
								<?php echo $testimonial->post_title; ?>
							</div>
						</div>

					<?php
					endforeach;
					?>
				</div>
			</div>
		</div>
	</div>
</div>

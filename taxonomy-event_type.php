<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Twice_Agency
 */

get_header();
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php 
			$args = array(
				'post_type' => 'events',
				'posts_per_page' => '-1',
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'tax_query' => array(
					array(
							'taxonomy' => 'event_type',
							'field' => 'slug',
							'terms' => $term->slug,
					)
			),
				
			);

			$loop = new WP_Query( $args );
			if ($loop->have_posts() > 0) : ?>
			<!-- Hero Section -->

			<div class=" hero-slider hide-mobile">
				<div class="sep-slider-inner owl-carousel">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				  <div class="sep-hero-slide" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_id(), 'full'); ?>);">
							<div class="sep-description-box">
								<div class="sep-description-box-inner">
									<h2 class="sep-title"><?php the_title(); ?></h2>
									<div class="intro__sep">
										<?php echo twice_sep_small(); ?>
									</div>
									<div class="sep-des-txt"><?php the_content(); ?></div>
								</div>
							</div>
					</div>
		<?php endwhile; ?>
				</div>

			</div>

			<!-- Hero Section End-->
			<?php
				endif;
				wp_reset_query();
			?>


<?php 
			$args = array(
				'post_type' => 'events',
				'posts_per_page' => '-1',
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'tax_query' => array(
					array(
							'taxonomy' => 'event_type',
							'field' => 'slug',
							'terms' => $term->slug,
					)
			),
				
			);

			$loop = new WP_Query( $args );
			if ($loop->have_posts() > 0) : ?>
<section class="single-events-stacked display-mobile-only">
	<div class="single-events-stacked-inner">
	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<div class="single-event mb-5 ">
			<div class="single-event-inner">
				<img src="<?php echo get_the_post_thumbnail_url(get_the_id(), 'full'); ?>" alt="">

				<div class="sep-description-box-inner txt-center mt-3 mb-3">
					<h2 class="sep-title"><?php the_title(); ?></h2>
					<div class="intro__sep ">
						<?php echo twice_sep_small(); ?>
					</div>
					<div class="sep-des-stacked-txt  ">
						<?php the_content(); ?>
					</div>
				</div>

			</div>
		</div>
		<?php endwhile; ?>



	</div>
</section>
		<?php 
		endif;
		wp_reset_query(); ?>






		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
